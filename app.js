const express = require('express')

const sql = require('mssql')

const app = express()
app.use(express.static('public'))
app.set('view engine', 'ejs')

const serverConfig = require('./config/server')

app.get('/server/:name', async (req, res) => {
  const serverName = req.params.name
  if (!Object.keys(bdConfig).includes(serverName)) {
    res.status(404).send('<h1>Servidor não consta lista máquinas!</h1>')
  } else {
    try {
      const result = await getStatus(serverName)
      const bdStatus = result.recordsets[0].map(bd => ({
        name: bd.name,
        status: bd.state_desc
      }))
      res.render('index', {
        bd: bdStatus,
        server: bdConfig[serverName].server,
        active: bdStatus.reduce((online, bd) => {
          return bd.status.toUpperCase() === 'ONLINE' ? online + 1 : online + 0
        }, 0)
      })
    } catch (error) {
      console.log('error', result.error)
      res.status(500).json({ error: result.error })
    }
  }
})

app.listen(serverConfig.port, () => {
  console.log(`Listening on port: ${serverConfig.port}! 
  go to: http://localhost:${serverConfig.port}/`)
})

const bdConfig = require('./config/bd')

const getStatus = async name => {
  try {
    const pool = await sql.connect(bdConfig[name])
    const result = await pool.request().query`SELECT * FROM sys.databases`
    pool.close()
    sql.close()
    return result
  } catch (err) {
    console.error('Erro BD', err.stack)
    throw new Error(err)
  }
}

sql.on('error', err => {
  console.error('Erro:', err.stack)
  sql.close()
  process.exit(1)
})
