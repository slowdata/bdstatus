# Simple APP to check BD status

## Dependencies
  Node.js


## Usage
Create a *bd.js* file inside *config* folder. File example:
```
module.exports = {
    servername: {
      user: '',
      password: '',
      server: '', // You can use 'localhost\\instance' to connect to named instance
      database: '', // Should be empty
      domain: '',
      options: {
        trustedConnection: true \\ For windows Authentication
      }
  }
}
```
Open command line and run `git clone https://slowdata@bitbucket.org/slowdata/bdstatus.git`
Then enter the repository directory: `cd bdstatus`

After that run: `npm install`

*To run the app* just run: `npm start`

###Usage:
(see output address)
http://localhost:3000/server/:name *http//localhost:3000/server/ssdevelop04
**The name of the server should be exactly the same as in the bd.js configuration**